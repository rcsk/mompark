package com.vlad.android.kotlin

public inlineOptions(InlineOption.ONLY_LOCAL_RETURN) fun async(action: () -> Unit): Unit = Thread(runnable(action)).start()

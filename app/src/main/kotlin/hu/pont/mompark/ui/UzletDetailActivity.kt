package hu.pont.mompark.ui

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks
import com.github.ksoichiro.android.observablescrollview.ScrollState
import com.github.ksoichiro.android.observablescrollview.ScrollUtils
import com.nineoldandroids.view.ViewHelper

import hu.pont.mompark.R
import android.widget.TextView
import android.text.Html
import hu.pont.mompark.model.Uzlet

public class UzletDetailActivity : BaseActivity(), ObservableScrollViewCallbacks {

    private var mImageView: View? = null
    private var mToolbarView: View? = null
    private var mScrollView: ObservableScrollView? = null
    private var mParallaxImageHeight: Int = 0
    private var mDetailTextView: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super<BaseActivity>.onCreate(savedInstanceState)
        setContentView(R.layout.activity_uzlet_detail)

        setSupportActionBar(findViewById(R.id.toolbar) as Toolbar)

        getActionBar()?.setDisplayHomeAsUpEnabled(true)
        getActionBar()?.setDisplayShowHomeEnabled(true)

        mImageView = findViewById(R.id.image)
        mToolbarView = findViewById(R.id.toolbar)
        mToolbarView!!.setBackgroundColor(ScrollUtils.getColorWithAlpha(0F, getResources().getColor(R.color.primary)))
        mDetailTextView = findViewById(R.id.body) as TextView
        mScrollView = findViewById(R.id.scroll) as ObservableScrollView
        mScrollView!!.setScrollViewCallbacks(this)

        mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen.parallax_image_height)

        mDetailTextView!!.setText(Html.fromHtml(Uzlet.PAULANER_HTML_DESC))
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super<BaseActivity>.onRestoreInstanceState(savedInstanceState)
        onScrollChanged(mScrollView!!.getCurrentScrollY(), false, false)
    }

    override fun onScrollChanged(scrollY: Int, firstScroll: Boolean, dragging: Boolean) {
        val baseColor = getResources().getColor(R.color.primary)
        val alpha = 1 - Math.max(0, mParallaxImageHeight - scrollY).toFloat() / mParallaxImageHeight.toFloat()
        mToolbarView!!.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor))
        ViewHelper.setTranslationY(mImageView, (scrollY / 2).toFloat())
    }

    override fun onDownMotionEvent() {
    }

    override fun onUpOrCancelMotionEvent(scrollState: ScrollState) {
    }
}
package hu.pont.mompark.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import hu.pont.mompark.R
import android.graphics.Bitmap

import java.util.ArrayList
import hu.pont.mompark.model.Uzlet
import hu.pont.mompark.adapter.UzletAdapter
import android.widget.ListView
import android.graphics.Canvas
import android.support.v4.app.Fragment
import android.content.Intent
import hu.pont.mompark.ui.UzletDetailActivity
import yalantis.com.sidemenu.interfaces.ScreenShotable
import hu.pont.mompark.ui.UzletDetailActivityCool
import com.vlad.android.kotlin.async

public class UzletListFragment : Fragment(), ScreenShotable {

    private var containerView: View? = null
    private var bitmap: Bitmap? = null

    protected var mUzletListView: ListView? = null

    val uzletList = ArrayList<Uzlet>()
    var uzletAdapter: UzletAdapter? = null
    var uzletRootView: View? = null

    override fun takeScreenShot() {
        async {
            val bitmap = Bitmap.createBitmap(containerView!!.getWidth(), containerView!!.getHeight(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            containerView!!.draw(canvas)
            this@UzletListFragment.bitmap = bitmap
        }
    }

    override fun getBitmap(): Bitmap? {
        return bitmap
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super<Fragment>.onCreate(savedInstanceState)

        uzletList.add(Uzlet("McDonald's", R.drawable.meki, R.drawable.header_mcdonalds, Uzlet.PAULANER_HTML_DESC, R.drawable.logo_meki))
        uzletList.add(Uzlet("Deichmann", R.drawable.decihman,R.drawable.header_deichmann, Uzlet.PAULANER_HTML_DESC, R.drawable.logo_deichmann))
        uzletList.add(Uzlet("Silver Islands", R.drawable.silverislands,R.drawable.header_silverislands, Uzlet.PAULANER_HTML_DESC, R.drawable.logo_silverislands))
        uzletList.add(Uzlet("Paulaner", R.drawable.paulaner,R.drawable.header_paulaner, Uzlet.PAULANER_HTML_DESC, R.drawable.logo_paulaner))
        uzletList.add(Uzlet("Griff", R.drawable.griff,R.drawable.header_griff, Uzlet.GRIFF_HTML_DESC, R.drawable.logo_griff))

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        uzletRootView = inflater.inflate(R.layout.fragment_uzlet_list, container, false)



        return uzletRootView
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super<Fragment>.onViewCreated(view, savedInstanceState)
        this.mUzletListView = view.findViewById(R.id.uzletListView) as ListView
        this.containerView = view.findViewById(R.id.container)
        uzletAdapter = UzletAdapter(getActivity(), R.layout.list_item_uzlet, uzletList)
        mUzletListView!!.setAdapter(uzletAdapter)

        mUzletListView!!.setOnItemClickListener { parent, view, position, id ->
            var uzlet: Uzlet? = ((parent as ListView)?.getAdapter() as UzletAdapter)?.getItem(position)
            val intent: Intent = Intent(getActivity(),javaClass<UzletDetailActivityCool>())
            intent!!.putExtra(UzletDetailActivityCool.ARG_CONTENT, uzlet?.getContentHtmlDesc())
            intent!!.putExtra(UzletDetailActivityCool.ARG_TITLE, uzlet?.getName())
            intent!!.putExtra(UzletDetailActivityCool.ARG_HEADER_RES, uzlet?.getHeaderResId() as Int)
            intent!!.putExtra(UzletDetailActivityCool.ARG_LOGO, uzlet?.getLogoResId() as Int)
            getActivity()?.startActivity(intent)
        }
        
    }


    class object {
        public val UZLETLIST: String = "UzletList"

    }

}

package hu.pont.mompark.ui.fragment

import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView


import hu.pont.mompark.R
import yalantis.com.sidemenu.interfaces.ScreenShotable

/**
 * Created by Konstantin on 22.12.2014.
 */
public class ContentFragment : Fragment(), ScreenShotable {

    private var containerView: View? = null
    protected var mImageView: ImageView? = null
    protected var res: Int = 0
    private var bitmap: Bitmap? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super<Fragment>.onViewCreated(view, savedInstanceState)
        this.containerView = view.findViewById(R.id.container)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super<Fragment>.onCreate(savedInstanceState)
        res = getArguments().getInt(javaClass<Int>().getName())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val rootView = inflater.inflate(R.layout.fragment_main, container, false)
        mImageView = rootView.findViewById(R.id.image_content) as ImageView
        mImageView!!.setClickable(true)
        mImageView!!.setFocusable(true)
        mImageView!!.setImageResource(res)
        return rootView
    }

    override fun takeScreenShot() {
        val thread = object : Thread() {
            override fun run() {
                val bitmap = Bitmap.createBitmap(containerView!!.getWidth(), containerView!!.getHeight(), Bitmap.Config.ARGB_8888)
                val canvas = Canvas(bitmap)
                containerView!!.draw(canvas)
                this@ContentFragment.bitmap = bitmap
            }
        }
        thread.start()
    }

    override fun getBitmap(): Bitmap? {
        return bitmap
    }

    class object {
        public val CLOSE: String = "Close"
        public val BUILDING: String = "Building"
        public val BOOK: String = "Book"
        public val PAINT: String = "Paint"
        public val CASE: String = "Case"
        public val SHOP: String = "Shop"
        public val PARTY: String = "Party"
        public val MOVIE: String = "Movie"

        public fun newInstance(resId: Int): ContentFragment {
            val contentFragment = ContentFragment()
            val bundle = Bundle()
            bundle.putInt(javaClass<Int>().getName(), resId)
            contentFragment.setArguments(bundle)
            return contentFragment
        }
    }
}


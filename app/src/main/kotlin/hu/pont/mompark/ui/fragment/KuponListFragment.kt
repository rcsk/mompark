package hu.pont.mompark.ui.fragment


import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import hu.pont.mompark.R
import yalantis.com.sidemenu.interfaces.ScreenShotable
import android.graphics.Bitmap
import android.support.v4.app.Fragment
import android.widget.ListView
import java.util.ArrayList
import hu.pont.mompark.model.Kupon
import hu.pont.mompark.adapter.KuponAdapter
import android.graphics.Canvas
import com.vlad.android.kotlin.async

/**
 * A simple {@link Fragment} subclass.
 */
public class KuponListFragment : Fragment(), ScreenShotable {

    private var containerView: View? = null

    private var bitmap: Bitmap? = null

    protected var mKuponListView: ListView? = null

    val kuponList = ArrayList<Kupon>()
    var kuponAdapter: KuponAdapter? = null
    var kuponRootView: View? = null

    override fun takeScreenShot() {
        async {
            val bitmap = Bitmap.createBitmap(containerView!!.getWidth(), containerView!!.getHeight(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            containerView!!.draw(canvas)
            this@KuponListFragment.bitmap = bitmap
        }
    }


    override fun getBitmap(): Bitmap? {
        return bitmap
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super<Fragment>.onCreate(savedInstanceState)

        kuponList.add(Kupon("Kupon 1", R.drawable.kupon1))
        kuponList.add(Kupon("Kupon 2", R.drawable.kupon2))
        kuponList.add(Kupon("Kupon 4", R.drawable.kupon4))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        kuponRootView = inflater.inflate(R.layout.fragment_kupon_list, container, false)
        return kuponRootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super<Fragment>.onViewCreated(view, savedInstanceState)
        this.mKuponListView = view.findViewById(R.id.kuponListView) as ListView
        this.containerView = view.findViewById(R.id.container)
        kuponAdapter = KuponAdapter(getActivity(), R.layout.list_item_kupon, kuponList)
        mKuponListView!!.setAdapter(kuponAdapter)

    }

    class object {
        public val KUPONLIST: String = "KuponList"
    }


}

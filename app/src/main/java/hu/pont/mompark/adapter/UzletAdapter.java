package hu.pont.mompark.adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.List;

import hu.pont.mompark.R;
import hu.pont.mompark.model.Uzlet;

/**
 * Created by Richard Radics on 2015.02.03..
 */
public class UzletAdapter extends ArrayAdapter<Uzlet> {


    Context context;
    List<Uzlet> uzletList;

    public UzletAdapter(Context context, int resource, List<Uzlet> objects) {
        super(context, resource, objects);

        this.context = context;
        this.uzletList = objects;

    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Uzlet current = uzletList.get(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.list_item_uzlet, null);

        ImageView imageView = (ImageView)convertView.findViewById(R.id.uzletListItemImageView);

        imageView.setImageResource(current.getResId());

        return convertView;
    }
}

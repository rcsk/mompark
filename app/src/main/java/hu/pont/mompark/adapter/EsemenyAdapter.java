package hu.pont.mompark.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.List;

import hu.pont.mompark.R;
import hu.pont.mompark.model.Esemeny;
import hu.pont.mompark.model.Uzlet;

/**
 * Created by Richard Radics on 2015.02.03..
 */
public class EsemenyAdapter extends ArrayAdapter<Esemeny> {


    Context context;
    List<Esemeny> esemenyList;

    public EsemenyAdapter(Context context, int resource, List<Esemeny> objects) {
        super(context, resource, objects);

        this.context = context;
        this.esemenyList = objects;

    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Esemeny current = esemenyList.get(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.list_item_esemeny, null);

        ImageView imageView = (ImageView)convertView.findViewById(R.id.esemenyListItemImageView);

        imageView.setImageResource(current.getResId());

        return convertView;
    }
}
